// LU: if i will have more time I will make the code easier to be reused

//LU: waits until the dom in loaded
window.onload = function(){
	var addBtn = document.getElementById("addBtn");
	var ul = document.getElementById("productList");

	// LU: load the products from localStorage
	var ini = function(){
		var data = window.localStorage.getItem('products');
		createProducts(JSON.parse(data));
	}

	// LU: get the product name and add to the list
	var addProduct = function(e){
		var productName = document.getElementById("productName").value;
		createProducts([productName]);		
	};

	// LU: creating the prodcuts from an array
	var createProducts = function(arg_products){
		for (var i = 0; i < arg_products.length; i++) {
			var deleteBtn = document.createElement("input");
			var li = document.createElement("li");
			var text = document.createTextNode(arg_products[i]);
			
			deleteBtn.setAttribute("type", "button");
			deleteBtn.setAttribute("value", "Delete");
			deleteBtn.addEventListener("click", deleteProduct);

			li.appendChild(text);
			li.appendChild(deleteBtn);
			ul.appendChild(li);
		};
		updateList();
	};

	//LU: delete the product from the list
	var deleteProduct = function(e){
		var li = e.target.parentNode;
		ul.removeChild(li);

		updateList();
	}

	//LU: updating the counting of the list
	var updateList = function(){
		var productsName = [];
		var countList = document.getElementById("countList");
		var products = document.querySelectorAll('#productList li');
		var items = (products.length==1) ? "item":"items";
		countList.innerHTML = products.length +" "+ items +" in the list";

		for (var i = 0; i < products.length; i++) {
			productsName.push(products[i].textContent);
		};

		//Save to LocalStorage
		window.localStorage.setItem('products',  JSON.stringify(productsName));
	}
	
	//LU: adding the click event listener
	addBtn.addEventListener("click", addProduct);

	ini();	
};